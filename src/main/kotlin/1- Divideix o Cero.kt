import java.util.Scanner

val scanner = Scanner(System.`in`)
fun main(){

    println("Introduce el primer numero")
    val num1 = scanner.nextInt()
    println("Introduce el segundo numero")
    val num2 = scanner.nextInt()

    dividir(num1,num2)
}
fun dividir(num1:Int, num2:Int){
    var resultat = 0
    try {
        resultat = num1 / num2
        println("Divisio: $num1/$num2: $resultat}")
    } catch (e: ArithmeticException) {
        println("Error: Divisió per zero")
    }
}