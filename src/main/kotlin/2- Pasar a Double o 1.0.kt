import java.lang.NumberFormatException

fun main(){
    println("Introduce valor")
    val param = scanner.next()

    println(aDoubleoU(param))
}

fun aDoubleoU(param:String):Double{
    try {
        return param.toDouble()
    }catch (e: NumberFormatException){
        return 1.0
    }
}