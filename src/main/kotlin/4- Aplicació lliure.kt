val ageOfPerson = mutableListOf<Int>()
fun main(){
    println("Introduce una opcion")
    println("1. Añadir edad")
    println("2. Eleminar edad")
    val menuOption = scanner.nextInt()
    if (menuOption == 1) addAge()
    else removeAge()
}
fun addAge(){
    println()
    println("Introduce edad a añadir:")
    val newAge = scanner.nextInt()
    ageOfPerson.add(newAge)

    print("Edades: ")
    for (i in ageOfPerson.indices){
        print("${ageOfPerson[i]}, ")
    }
    println()
    main()
}
fun removeAge(){
    try {
        println()
        println("Introduce posicion a eliminar:")
        val age = scanner.nextInt()

        ageOfPerson.remove(ageOfPerson[age])
    }catch (e: IndexOutOfBoundsException ){
        println("Posicion inexistente")
        println()
    }
    print("Edades: ")
    for (i in ageOfPerson.indices){
        print("${ageOfPerson[i]}, ")
    }
    println()
    main()
}