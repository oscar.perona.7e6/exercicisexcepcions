import java.io.IOException
import kotlin.io.path.Path
import kotlin.io.path.readLines

fun main(){
    readFIle()
}
fun readFIle(){
    val file = Path("./src/main/kotlin/ex3Txt.txt")
    try {
        val fileRead = file.readLines()
        for (line in fileRead){
            println(line)
        }
    }catch (e: IOException){
        println("S'ha produït un error d'entrada/sortida: $e")
    }
}